var antes = [
	'Profesional para evitar los retos',
	'Esa persona que se deja llevar por el viento',
	'Inconsciente del dolor ajeno',
	'Una persona encadenada a la rutina',
	'la famosa página en blanco',
	'resistente al cambio y a las sugerencias',
	'Quien presumía del conocimiento',
	'La sombra de mis compañeros',
	'Una sola historia, un solo camino',
	'Una persona llena de dudas',
	'Un ser sin memoria',
	'Esa persona que solo se enfocaba en alcanzar metas',
	'Ese ser callado que nunca opina por temor al rechazo',
	'Una copia de mis colegas',
	'Una persona con miedo a preguntar',
	'El incierto rumbo de quien trabaja sin propósito',
	'Quien especulaba y no preguntaba',
	'Un rompecabezas incompleto',
	'Un ser hermético que sufría por lo que callaba',
	'Fanfarrón de mis éxitos',
	'Profesional sin tacto a la hora de hablar',
	'Un individuo que anteponía los resultados a las personas',
	'Solo un ejecutador de tareas',
	'Profesional para evitar los retos',
	'La sombra de mis compañeros',
	'Inconsciente del dolor ajeno',
	'Una persona encadenada a la rutina',
	'Esa persona que nunca opina',
	'Alguien que trabajaba sin propósito',
	'Un trabajador cerrado a mi campo',
	'Un ser listo para competir y ganar a costa de todo',
	'Muy sensible para recibir consejo y recomendaciones',
	'Impaciente para explicar ideas y conceptos'
]

var despues = [
	'Valiente para enfrentar lo que sea',
	'Una persona que resiste la crítica',
	'Un ser de luz y enfoque',
	'Representante de la vida',
	'Libre para amar lo que soy',
	'Capaz de aprender del fracaso',
	'Esa persona que escucha a otros y aprende',
	'Un ser con una misión trascendente',
	'El conjunto de historias que tocaron mi vida',
	'Generador de esperanza',
	'Una voz que se pronuncia y sana',
	'Un libro de preguntas y respuestas que se sigue escribiendo',
	'Libre para dar mis sugerencias y aportar a otros',
	'Parte de una comunidad transformadora',
	'Agente de paz',
	'El sueño de una nación renovada',
	'Aguante, resistencia y resiliencia',
	'Quien aporta a la construcción del respeto y la tolerancia',
	'Ese ser que se define por su esencia y no solo por lo que hace',
	'Como ese niño agradecido que disfruta jugar con cajas',
	'Un promotor de paz incluso desde mi modo de hablar',
	'Responsable de mi alegría',
	'Quien hace reír a otros aunque el día esté feo',
	'Una hebra del tejido social',
	'Parte de un dispositivo humano y tecnológico para la investigación',
	'Una persona que trabaja en comunidad con satisfacción',
	'Alguien curioso e inquieto por resolver problemas',
	'Una persona integral que ama trabajar con todo tipo de profesionales',
	'Un ser que construye con y para todos',
	'Quien comparte conocimiento y crece en el proceso',
	'Un instrumento de reconciliación',
	'Paciente en la resistencia, objetivo con los comentarios en pro de mi crecimiento',
	'Un comunicador de la vida'
]

function mostrar() {
	var num = Math.floor(Math.random() * (antes.length - 1));
	var num2= Math.floor(Math.random() * (antes.length - 1));
	document.getElementById('cita-antes').innerHTML = antes[num];
	document.getElementById('cita-despues').innerHTML = despues[num2];
}

setInterval(() => {
	var num = Math.floor(Math.random() * 24) + 1;
	var src = 'img/' + num + '.jpg'
	document.getElementById('img').src=src;
  }, 2000);
